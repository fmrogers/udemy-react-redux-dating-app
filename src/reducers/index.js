// Import modules
import { combineReducers } from 'redux';

// Import reducers
import BooksReducer from './reducer_books';
import ActiveBook from './reducer_activie_book';

// Globbal Aplication State
const rootReducer = combineReducers({
    books: BooksReducer,
    activeBook: ActiveBook
});

export default rootReducer;
